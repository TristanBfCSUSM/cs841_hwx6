﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Plugin.Connectivity;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using Homework_6;

namespace Homework_6
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        public bool DoIHaveInternet()
        {
            return CrossConnectivity.Current.IsConnected;
        }


        //Clicked on search button will do a GET request to get more informations about the word
        async void Button_Clicked(System.Object sender, System.EventArgs e)
        {
            var wordchoose = WordSearched.Text;

            //Verifiry if field is empty or not
            if (string.IsNullOrEmpty(wordchoose))
            {
                await DisplayAlert("Empty fill", "Fill with a word to search please", "Ok");
                return;
            }

            //Verify internet connection before API request
            if (!DoIHaveInternet())
            {
                await DisplayAlert("No Connection", "You need to be connected to internet for searching a word", "Ok");
                return;
            }

            //clear informations
            clearAllProps();

            //Do request
            HttpClient client = new HttpClient();

            var uri = new Uri(
                string.Format(
                    $"https://owlbot.info/api/v4/dictionary/" + $"{wordchoose}"));
            var httpMethod = HttpMethod.Get;
            var request = new HttpRequestMessage()
            {
                Method = httpMethod,
                RequestUri = uri,
            };
            request.Headers.Authorization = new AuthenticationHeaderValue("Token", "601675869193d9e396edab07cff33690198c4fe9");
            HttpResponseMessage response = await client.SendAsync(request);

            //Verifiy response
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                try
                {
                    var welcome = WordSearchedJson.FromJson(content);
                    if (string.IsNullOrEmpty(welcome.Word))
                    {
                        await DisplayAlert("Bad word", "We havn't found a definition for the word " + welcome.Word + ". Please, try again and verify if you write it right.", "Ok");
                        return;
                    }

                    //transform first letter in uppercase for the definition
                    char[] letter = welcome.Definitions[0].DefinitionDefinition.ToCharArray();
                    letter[0] = char.ToUpper(letter[0]);
                    welcome.Definitions[0].DefinitionDefinition = new string(letter);
                    
                    Wordchoose.Text = welcome.Word;
                    TypeW.Text = welcome.Definitions[0].Type;
                    DefinitionW.Text = welcome.Definitions[0].DefinitionDefinition;
                    if (!string.IsNullOrEmpty(welcome.Definitions[0].Example))
                        ExampleW.Text = "Example : " + welcome.Definitions[0].Example;
                    ImageW.Source = welcome.Definitions[0].ImageUrl;
                    EmojiW.Text = welcome.Definitions[0].Emoji;
                    ClearButton.IsVisible = true;
                    Separation.IsVisible = true;

                }
                catch (Exception ex)
                {
                    Debug.WriteLine("We had a problem " + ex.Message.ToString());
                }
            }
            else
                await DisplayAlert("Bad word", "We havn't found a definition for the word " + wordchoose + ". Please, try again and verify if you write it right.", "Ok");
        }

        //Clear all fields before new search
        void clearAllProps()
        {
            Wordchoose.Text = "";
            TypeW.Text = "";
            DefinitionW.Text = "";
            ExampleW.Text = "";
            ImageW.Source = "";
            EmojiW.Text = "";
            ClearButton.IsVisible = false;
            Separation.IsVisible = false;
            WordSearched.Text = "";
        }

        void Button_Clicked_1(System.Object sender, System.EventArgs e)
        {
            clearAllProps();
        }
    }
}
