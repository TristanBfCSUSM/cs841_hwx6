﻿using System;
using System.Collections.Generic;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Homework_6
{
    //Class to get the Json after deserialize
    public partial class WordSearchedJson
    {
        [JsonProperty("definitions")]
        public Definition[] Definitions { get; set; }

        [JsonProperty("word")]
        public string Word { get; set; }

        [JsonProperty("pronunciation")]
        public string Pronunciation { get; set; }
    }

    public partial class Definition
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("definition")]
        public string DefinitionDefinition { get; set; }

        [JsonProperty("example")]
        public string Example { get; set; }

        [JsonProperty("image_url")]
        public Uri ImageUrl { get; set; }

        [JsonProperty("emoji")]
        public string Emoji { get; set; }
    }

    //Deserialize here
    public partial class WordSearchedJson
    {
        public static WordSearchedJson FromJson(string json) => JsonConvert.DeserializeObject<WordSearchedJson>(json, Homework_6.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this WordSearchedJson self) => JsonConvert.SerializeObject(self, Homework_6.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }

}
